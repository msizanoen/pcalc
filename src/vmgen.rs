use super::vm::Instruction;
use super::{Expr, Op};

#[derive(Default)]
pub struct VMGen {
    insns: Vec<Instruction>,
    rm: RegisterManager,
}

#[derive(Default)]
pub struct RegisterManager {
    cur_reg: u8,
    released: Vec<u8>,
}

impl RegisterManager {
    pub fn new() -> Self {
        RegisterManager {
            cur_reg: 0,
            released: Vec::new(),
        }
    }

    pub fn reserve(&mut self) -> u8 {
        if let Some(r) = self.released.pop() {
            r
        } else {
            let r = self.cur_reg;
            self.cur_reg += 1;
            r
        }
    }

    pub fn release(&mut self, reg: u8) {
        self.released.push(reg);
    }
}

impl VMGen {
    pub fn new() -> Self {
        VMGen::default()
    }
    fn emit(&mut self, i: Instruction) {
        self.insns.push(i);
    }

    fn cur_pc(&self) -> usize {
        self.insns.len()
    }

    fn const_val(&mut self, n: i32) -> u8 {
        let reg = self.rm.reserve();
        self.emit(Instruction::Const(reg, n));
        reg
    }

    fn expr(&mut self, e: &Expr) -> u8 {
        match e {
            Expr::Literal(n) => self.const_val(*n),
            Expr::Op(Op::Add, l, r) => self.op_add(l, r),
            Expr::Op(Op::Sub, l, r) => self.op_sub(l, r),
            Expr::Op(Op::Mul, l, r) => self.op_mul(l, r),
            Expr::Op(Op::Div, l, r) => self.op_div(l, r),
        }
    }

    fn op_add(&mut self, l: &Expr, r: &Expr) -> u8 {
        let l_r = self.expr(l);
        let r_r = self.expr(r);
        let res = self.rm.reserve();
        self.emit(Instruction::Add(res, l_r, r_r));
        self.rm.release(l_r);
        self.rm.release(r_r);
        res
    }

    fn op_sub(&mut self, l: &Expr, r: &Expr) -> u8 {
        let l_r = self.expr(l);
        let r_r = self.expr(r);
        let res = self.rm.reserve();
        self.emit(Instruction::Sub(res, l_r, r_r));
        self.rm.release(l_r);
        self.rm.release(r_r);
        res
    }

    fn op_mul(&mut self, l: &Expr, r: &Expr) -> u8 {
        let l_r = self.expr(l);
        let r_r = self.expr(r);
        let res = self.rm.reserve();
        self.emit(Instruction::Mul(res, l_r, r_r));
        self.rm.release(l_r);
        self.rm.release(r_r);
        res
    }

    fn op_div(&mut self, l: &Expr, r: &Expr) -> u8 {
        let l_r = self.expr(l);
        let r_r = self.expr(r);
        let res = self.rm.reserve();
        self.emit(Instruction::Div(res, l_r, r_r));
        self.rm.release(l_r);
        self.rm.release(r_r);
        res
    }

    pub fn main_expr(&mut self, e: &Expr) {
        let e_r = self.expr(e);
        self.emit(Instruction::Return(e_r));
        self.rm.release(e_r);
    }

    pub fn get_insns(&self) -> &Vec<Instruction> {
        &self.insns
    }
}
