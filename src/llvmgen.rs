use super::Expr;
use super::Op;

pub struct LLVMGenerator {
    body: String,
    cur_num: u32,
}

fn op_name(o: &Op) -> String {
    match o {
        Op::Add => "add",
        Op::Sub => "sub",
        Op::Mul => "mul",
        Op::Div => "sdiv",
    }
    .to_owned()
}

impl Default for LLVMGenerator {
    fn default() -> Self {
        LLVMGenerator::new()
    }
}
impl LLVMGenerator {
    pub fn new() -> Self {
        LLVMGenerator {
            body: String::new(),
            cur_num: 0,
        }
    }

    fn uident(&mut self, prefix: &str) -> String {
        let cur = self.cur_num;
        self.cur_num += 1;

        format!("{}{}", prefix, cur)
    }

    fn emit(&mut self, s: &str) {
        self.body.push_str(s);
    }

    fn literal_i32(&mut self, val: i32) -> String {
        format!("{}", val)
    }

    fn expr(&mut self, e: &Expr) -> String {
        match e {
            Expr::Literal(i) => self.literal_i32(*i),
            Expr::Op(op, l, r) => self.op(op, l, r),
        }
    }

    fn op(&mut self, o: &Op, l: &Expr, r: &Expr) -> String {
        let l_i = self.expr(l);
        let r_i = self.expr(r);
        let result = self.uident(&op_name(o));
        let code = format!(
            r###"    %{} = {} i32 {}, {};
"###,
            result,
            op_name(o),
            l_i,
            r_i
        );
        self.emit(&code[..]);
        format!("%{}", result)
    }

    pub fn finalize(&mut self, e: &Expr) -> String {
        let resvar = self.expr(e);
        let code = format!(
            r###"    ret i32 {};
"###,
            resvar
        );
        self.emit(&code[..]);
        format!(
            r###"define i32 @run() {{
{}}}
"###,
            self.body
        )
    }
}
