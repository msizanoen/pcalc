#[derive(Debug, Copy, Clone)]
pub enum Instruction {
    Nop,
    Return(u8),
    If(u8, usize, usize),
    Br(usize),
    Neg(u8, u8),
    Add(u8, u8, u8),
    Sub(u8, u8, u8),
    Mul(u8, u8, u8),
    Div(u8, u8, u8),
    Const(u8, i32),
}

pub fn run(i: &[Instruction]) -> i32 {
    let mut pc = 0;
    let mut regs = [0; 256];

    while pc < i.len() {
        match *(unsafe { i.get_unchecked(pc) }) {
            Instruction::Nop => (),
            Instruction::Return(u) => return regs[u as usize],
            Instruction::Const(r, u) => {
                regs[r as usize] = u;
            }
            Instruction::Add(i, l, r) => {
                regs[i as usize] = regs[l as usize] + regs[r as usize];
            }
            Instruction::Sub(i, l, r) => {
                regs[i as usize] = regs[l as usize] - regs[r as usize];
            }
            Instruction::Mul(i, l, r) => {
                regs[i as usize] = regs[l as usize] * regs[r as usize];
            }
            Instruction::Div(i, l, r) => {
                regs[i as usize] = regs[l as usize] / regs[r as usize];
            }
            _ => unimplemented!(),
        }

        pc += 1;
    }

    0
}
